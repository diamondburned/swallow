package desktop

import (
	"strings"
)

func iniString(m map[string]string, key string) string {
	v, ok := m[key]

	if !ok {
		return ""
	}

	return v
}

func splitArray(m map[string]string, key string) []string {
	s := iniString(m, key)
	if s == "" {
		return []string{}
	}

	return strings.Split(s, ";")
}

func parseBool(m map[string]string, key string) bool {
	switch iniString(m, key) {
	case "true", "True": // I don't see True, but eh
		return true
	default:
		return false
	}
}

func parseRegions(keys map[string]string, key string) map[string]string {
	m := make(map[string]string)

	replacer := strings.NewReplacer(
		key+"[", "",
		"]", "",
	)

	for k, v := range keys {
		if strings.HasPrefix(k, key) {
			m[replacer.Replace(k)] = v
		}
	}

	return m
}
