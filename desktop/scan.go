package desktop

import (
	"log"
	"os"
	"strings"
	"sync"
)

// ScanOpts is the struct for ScanDesktop options
// This is optional, you could pass in a nil ptr
type ScanOpts struct {
	Exclusion []string
}

func (so *ScanOpts) isInExclusion(path string) bool {
	if so == nil {
		return false
	}

	for _, p := range so.Exclusion {
		if strings.HasSuffix(path, p) {
			return true
		}
	}

	return false
}

// ScanDesktop scans all possible directories for Desktop Files
// and parse them
func ScanDesktop(opts *ScanOpts) (df Files) {
	df = []*Desktop{}

	wg := sync.WaitGroup{}

	for variable, path := range XDGVariables {
		values := os.Getenv(variable)
		if values == "" {
			continue
		}

		directories := strings.Split(values, ":")

		for _, dir := range directories {
			if opts.isInExclusion(dir + path) {
				continue
			}

			wg.Add(1)

			func() {
				defer wg.Done()

				ent, err := os.Open(dir + path)
				if err != nil {
					log.Println(err)
					return
				}

				files, err := ent.Readdirnames(-1)
				if err != nil {
					log.Println(err)
					return
				}

				for _, filename := range files {
					loc := dir + path + "/" + filename

					desktop := parseIni(loc)
					if desktop != nil {
						desktop.Path = loc
						df = append(df, desktop)
					}
				}
			}()
		}
	}

	wg.Wait()

	return
}
