package desktop

var (
	// XDGVariables contains all the variables to query
	// for desktop files
	XDGVariables = map[string]string{
		"XDG_DATA_HOME": "/applicatons",
		"HOME":          "/.local/share/applications",
		"XDG_DATA_DIRS": "/applications",
	}
)

// Files contains multiple Desktop files
type Files []*Desktop

// Desktop contains one desktop file
type Desktop struct {
	Path string

	/*
		To call the original entry from those
		maps, use the field name for the map
		key name

		Example: desktop.Name["Name"]
	*/

	Name        map[string]string
	GenericName map[string]string
	Comment     map[string]string
	Version     string

	Exec    string
	TryExec string

	Icon string
	Type string

	StartupNotify bool
	Terminal      bool

	Categories []string
	MimeType   []string

	Actions map[string]*Desktop
}
