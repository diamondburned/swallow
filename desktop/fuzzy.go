package desktop

import "strings"

// For use with github.com/sahilm/fuzzy

// Len returns array length
func (f Files) Len() int {
	return len(f)
}

// Name returns an extensive name
func (f Files) String(i int) (s string) {
	if v, ok := f[i].Name["Name"]; ok {
		s += v + " "
	}

	if v, ok := f[i].GenericName["GenericName"]; ok {
		s += v + " "
	}

	e := strings.Fields(f[i].Exec)
	if len(e) > 0 {
		s += e[0] + " "
	}

	return
}
