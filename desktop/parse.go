package desktop

import (
	"log"
	"strings"

	"github.com/davecgh/go-spew/spew"
	ini "gopkg.in/ini.v1"
)

var (
	// CommentKey is an easy way to parse
	// regions from Comment[region]
	CommentKey = strings.NewReplacer(
		"Comment[", "",
		"]", "",
	)

	iniOpts = ini.LoadOptions{
		IgnoreInlineComment:         true,
		IgnoreContinuation:          false,
		UnescapeValueCommentSymbols: true,
		AllowBooleanKeys:            true,
		SpaceBeforeInlineComment:    true,
		SkipUnrecognizableLines:     true,
	}
)

// TODAY, WE PARSE THIS
// THE BASH WAY
func parseIni(path string) *Desktop {
	data, err := ini.LoadSources(iniOpts, path)
	if err != nil {
		log.Println(err)
		return nil
	}

	e := data.Section("Desktop Entry")
	if e == nil {
		return nil
	}

	if path == "/usr/share/applications/google-chrome-unstable.desktop" {
		spew.Dump(e.Body())
	}

	desktop := parseSection(e)
	desktop.Actions = make(map[string]*Desktop)

	actionNames := splitArray(e.KeysHash(), "Actions")

	for _, a := range actionNames {
		s := data.Section("Desktop Action " + a)

		desktop.Actions[a] = parseSection(s)
	}

	return desktop
}

func parseSection(s *ini.Section) *Desktop {
	e := s.KeysHash()

	return &Desktop{
		Name:        parseRegions(e, "Name"),
		GenericName: parseRegions(e, "GenericName"),
		Comment:     parseRegions(e, "Comment"),
		Version:     iniString(e, "Version"),

		Exec:    iniString(e, "Exec"),
		TryExec: iniString(e, "TryExec"),

		Icon: iniString(e, "Icon"),
		Type: iniString(e, "Type"),

		StartupNotify: parseBool(e, "StartupNotify"),
		Terminal:      parseBool(e, "Terminal"),

		Categories: splitArray(e, "Categories"),
		MimeType:   splitArray(e, "MimeType"),
	}
}
