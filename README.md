# swallow

Lightweight app menu, like the one in LXQt

![Screenshot](https://ptpb.pw/KeUv.png)

## Features

- Icons
- Fuzzy search

## Todo

- [ ] Multithread scanning
- [ ] Ellipsis text wrap
- [ ] Have enter/select/whatever actually do something
- [ ] Add fzf
- [x] Make it look better or sth
- [x] Ctrl-A or die
- [x] Eliminate duplicates

## Planned behaviors

- Arrow down in input will give you second entry focus
- Enter in input will run first entry
- Typing when not focused on input will erase input before refocusing

## Things I won't do

- Transparency
	- If you're on GNOME, you could use CSS
	- Anything else and the gotk3 binding doesn't support it

## GTK CSS Guides

### Input box

```css
entry {
	/* CSS */
}
```

### List entry

```css
list > row > box {
	/* CSS */
}
```

#### Image

```css
list > row > box > image {
	/* CSS */
}
```

#### Text

```css
list > row > box > label {
	/* CSS */
}
```
