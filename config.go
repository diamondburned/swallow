package main

const (
	Width  = 250
	Height = 400

	// Spacing is the main box's spacing, or the distance
	// between the input and the list
	Spacing = 0

	// IconSize controls the list icon size
	IconSize = 24

	// CustomCSS lets you inject custom CSS
	CustomCSS = `
		entry {
			border: none;
			border-bottom-left-radius: 0px;
			border-bottom-right-radius: 0px;
			border-top-left-radius: 0px;
			border-top-right-radius: 0px;
			box-shadow: none;
			padding: 6px 8px;
			border-top: 1px solid rgb(24, 24, 24);
		}

		.background > box {
			border-top: 1px solid rgb(24, 24, 24);
			border-bottom: 1px solid rgb(24, 24, 24);
		}

		list > row > box {
			margin: 4px;
		}

		list > row > box > image {
			margin-right: 4px;
		}
	`
)

var (
	// ExclusionList is an array of excluded paths to
	// not search the desktop files in
	ExclusionList = []string{
		"/.local/share/applications",
	}
)
