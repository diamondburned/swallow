package main

import (
	"log"
	"os/exec"
	"strconv"
	"strings"
	"unsafe"

	"./desktop"
	"github.com/davecgh/go-spew/spew"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"github.com/sahilm/fuzzy"
)

/*
TODO
- flag to pick center, corner or on mouse

*/

func main() {
	df := desktop.ScanDesktop(&desktop.ScanOpts{
		Exclusion: ExclusionList,
	})

	// Initialize GTK without parsing any command line arguments.
	gtk.Init(nil)

	disp, err := gdk.DisplayGetDefault()
	if err != nil {
		log.Fatalln(err)
	}

	monitor, err := disp.GetPrimaryMonitor()
	if err != nil {
		log.Fatalln(err)
	}

	spew.Dump(monitor.GetGeometry().GetHeight())

	css, err := gtk.CssProviderNew()
	if err != nil {
		log.Fatalln(err)
	}

	if err := css.LoadFromData(CustomCSS); err != nil {
		log.Fatalln(err)
	}

	// Handling transparency
	//ctx := cairo.Create()
	//imgsurf := cairo.CreateImageSurface(cairo.FORMAT_ARGB32, Width, Height)
	//ctx := cairo.Create(imgsurf)
	//ctx.SetOperator(cairo.OPERATOR_SOURCE)

	// Create a new toplevel window, set its title, and connect it to the
	// "destroy" signal to exit the GTK main loop when it is destroyed.
	win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal("Unable to create window:", err)
	}

	win.SetTitle("swallow")
	win.SetPosition(gtk.WIN_POS_NONE)
	win.SetGravity(8)
	win.Move(0, monitor.GetGeometry().GetHeight())
	win.SetKeepAbove(true)
	win.SetDecorated(false)
	win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	win.SetAppPaintable(true)

	scr, err := win.GetScreen()
	if err != nil {
		log.Fatalln(err)
	}

	gtk.AddProviderForScreen(
		scr, css, 65535,
	)

	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)

	box.SetSpacing(Spacing)

	scroll, _ := gtk.ScrolledWindowNew(nil, nil)

	scroll.SetProperty("height-request", 250)
	scroll.SetPolicy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
	scroll.SetProperty("propagate-natural-width", true)
	scroll.SetProperty("propagate-natural-height", true)
	scroll.SetHExpand(true)
	scroll.SetVExpand(true)

	box.PackStart(scroll, true, true, 0)

	listbox, err := gtk.ListBoxNew()
	if err != nil {
		log.Fatalln(err)
	}

	listbox.SetActivateOnSingleClick(true)
	listbox.SetSelectionMode(gtk.SELECTION_SINGLE)
	listbox.Connect("row-activated", func(listbox *gtk.ListBox, row *gtk.ListBoxRow) {
		w, err := row.GetChild()
		if err != nil {
			log.Fatalln(err)
		}

		if w.IsA(box.TypeFromInstance()) {
			box := (*gtk.Box)(unsafe.Pointer(w))
			t, _ := box.GetTooltipText()
			i, e := strconv.Atoi(t)
			if e == nil {
				var (
					cmd  string
					args []string

					f []string
				)

				switch df[i].TryExec {
				case "":
					f = strings.Fields(df[i].Exec)
				default:
					f = strings.Fields(df[i].TryExec)
				}

				cmd = f[0]
				if len(f) > 1 {
					args = f[1:]
				}

				exec.Command(cmd, args...).Start()

				gtk.MainQuit()
			}
		}

		//Done:
	})

	for i, d := range df {
		if d.Name["Name"] == "" {
			continue
		}

		listbox.Add(craftList(d, i))

		i++
		if i > 15 {
			break
		}
	}

	scroll.Add(listbox)

	input, _ := gtk.EntryNew()
	input.Connect("changed", func() {
		listbox.GetChildren().Foreach(func(item interface{}) {
			if w, ok := item.(*gtk.Widget); ok {
				listbox.Remove(w)
			}
		})

		text, _ := input.GetText()

		results := fuzzy.FindFrom(text, df)

		e := 0
		for i := 0; i < len(results)-1; i++ {
			if results[i].Score == results[i+1].Score {
				continue
			}

			d := df[results[i].Index]

			if d.Name["Name"] == "" {
				continue
			}

			listbox.Add(craftList(d, results[i].Index))
			e++

			if e > 20 {
				break
			}
		}

		listbox.ShowAll()
	})

	input.SetCanFocus(true)
	input.SetProperty("placeholder-text", "Applications")
	input.SetProperty("activates-default", true)

	box.PackEnd(input, false, true, 0)

	win.Add(box)

	// Set the default window size.
	win.SetDefaultSize(Width, Height)

	// Recursively show all widgets contained in this window.
	win.ShowAll()

	win.SetOpacity(1)

	win.Connect("key-press-event", func(win *gtk.Window, ev *gdk.Event) {
		keyEvent := gdk.EventKeyNewFromEvent(ev)

		//                      v Esc keycode
		if keyEvent.KeyVal() == 65307 {
			gtk.MainQuit()
		}
	})

	// win.Connect("focus-out-event", gtk.MainQuit())

	input.GrabFocus()

	// Begin executing the GTK main loop.  This blocks until
	// gtk.MainQuit() is run.
	gtk.Main()
}

func craftList(d *desktop.Desktop, i int) *gtk.ListBoxRow {
	lbr, _ := gtk.ListBoxRowNew()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)

	box.SetTooltipText(strconv.Itoa(i))

	lbr.Add(box)

	image, _ := gtk.ImageNewFromIconName(d.Icon, gtk.ICON_SIZE_BUTTON)
	image.SetPixelSize(IconSize)
	box.Add(image)

	label, _ := gtk.LabelNew(d.Name["Name"])
	box.Add(label)

	return lbr
}
